package carExample.vehicles;

import carExample.details.Engine;
import carExample.professions.Driver;

public class Lorry extends Car{
    private double liftingCapacity;

    public double getLiftingCapacity() {
        return liftingCapacity;
    }

    public void setLiftingCapacity(double liftingCapacity) {
        this.liftingCapacity = liftingCapacity;
    }

    @Override
    public String toString() {
        return "Lorry{" +
                "liftingCapacity=" + liftingCapacity +
                "} " + super.toString();
    }

}
