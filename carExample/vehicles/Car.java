package carExample.vehicles;

import carExample.details.Engine;
import carExample.professions.Driver;

public class Car  {

    private String producer;
    private String aClass;
    private double weight;
    private Driver driver;
    private Engine engine;


    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getaClass() {
        return aClass;
    }

    public void setaClass(String aClass) {
        this.aClass = aClass;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void start() {
        System.out.println("Поехали");
    }

    public void stop() {
        System.out.println("Останавливаемся");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот налево");
    }

    public void turnHeadLights(){
        System.out.println("Фари включено");
    }

    public void turnOffHeadLights(){
        System.out.println("Фари вимкнуто");
    }

    public void turnTheTurnSignal(){
        System.out.println("Вімкнути поворитник");
    }

    public void turnOffTheTurnSignal(){
        System.out.println("Вимкнути поворотник");
    }

    public void turnBackLight(){
        System.out.println("Вімкнути підсвітку");
    }

    public void turnOffBackLight(){
        System.out.println("Вимкнути світло");
    }

    public void openTrunk(){
        System.out.println("Відкрити багажник");
    }

    public void closeTrunk(){
        System.out.println("Закрити багажник");
    }

    public void turnSportMode(){
        System.out.println("Режим спорт вімкнено");
    }

    public void turnCityMode(){
        System.out.println("Режим міський вімкнено");
    }

    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", aClass='" + aClass +  '\''+
                ", weight=" + weight +
                ", driver=" + driver +
                ", engine=" + engine +
                '}';
    }
}