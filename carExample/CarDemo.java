package carExample;

import carExample.details.Engine;
import carExample.professions.Driver;
import carExample.vehicles.Car;
import carExample.vehicles.Lorry;
import carExample.vehicles.SportCar;

public class CarDemo {
    public static void main(String[] args) {

        Driver bmwDriver = new Driver("Иванов В.В.", 50,  30);
        Engine bmvEngine = new Engine(60,"Bmw");
        Car car = new Car();
        car.setProducer("Bmw");
        car.setaClass("C");
        car.setWeight(5000);
        car.setDriver(bmwDriver);
        car.setEngine(bmvEngine);

        Driver lorryDriver = new Driver("Петров В.В.", 45,  20);
        Engine lorryEngine = new Engine(30, "LorryEngine");
        Lorry lorry = new Lorry();
        lorry.setProducer("Грузовик");
        lorry.setaClass("D");
        lorry.setWeight(8000);
        lorry.setDriver(lorryDriver);
        lorry.setEngine(lorryEngine);
        lorry.setLiftingCapacity(70);

        Driver sportDriver = new Driver("Сидоров В.В.", 30,  15);
        Engine sportEngine = new Engine(80, "SportEngine");
        SportCar sportCar = new SportCar();
        sportCar.setProducer("SportCar");
        sportCar.setaClass("C");
        sportCar.setDriver(sportDriver);
        sportCar.setEngine(sportEngine);
        sportCar.setSpeed(300);

        System.out.println(car);
        System.out.println(lorry);
        System.out.println(sportCar);


    }
}
